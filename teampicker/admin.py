from django.contrib import admin

from teampicker.models import Crowd, Team


class TeamInline(admin.TabularInline):
    model = Team
    max_num = 0

    def has_change_permission(self, *args, **kwargs) -> bool:
        return False


@admin.register(Crowd)
class CrowdAdmin(admin.ModelAdmin):
    readonly_fields = ("id", "created_by", "num_teams")
    inlines = (TeamInline,)
