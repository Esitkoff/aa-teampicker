from django.apps import AppConfig


class ExampleConfig(AppConfig):
    name = "teampicker"
    label = "teampicker"
    verbose_name = "Team Picker"
