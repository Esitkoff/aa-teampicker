import datetime as dt
import uuid

from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import Count, Min
from django.urls import reverse
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

from app_utils.urls import reverse_absolute

from teampicker.app_settings import TEAMPICKER_CROWD_STALE_HOURS
from teampicker.managers import CrowdManager


class General(models.Model):
    """Meta model for app permissions"""

    class Meta:
        managed = False
        permissions = (
            ("basic_access", _("Can access this app")),
            ("manage_crowds", _("Can manage crowds")),
        )


class Crowd(models.Model):
    """Crowd consisting of multiple teams."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    created_at = models.DateTimeField(auto_now_add=True, db_index=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="+")
    name = models.CharField(_("name"), max_length=100)
    description = models.TextField(_("description"), default="", blank=True)
    num_teams = models.PositiveIntegerField(
        _("number of teams"), validators=[MinValueValidator(2), MaxValueValidator(26)]
    )
    max_members = models.PositiveIntegerField(
        _("max members"),
        default=3,
        validators=[MinValueValidator(1)],
        help_text="Max. number of members one user can have.",
    )

    objects = CrowdManager()

    def __str__(self) -> str:
        return self.name

    def save(self, *args, **kwargs) -> None:
        super().save(*args, **kwargs)
        teams = []
        for num in range(self.num_teams):
            teams.append(Team(crowd=self, num=num))
        Team.objects.bulk_create(teams)

    def get_absolute_url(self):
        return reverse("teampicker:crowd-detail", kwargs={"pk": self.pk})

    def my_teams_url(self):
        return reverse_absolute("teampicker:crowd-join", args=[self.pk])

    def join_random_team(self, user: User) -> "Team":
        memberships = TeamMember.objects.filter(team__crowd=self, user=user).count()
        if memberships == self.max_members:
            raise OverflowError()
        teams = self.teams.annotate(num_members=Count("members__pk"))
        min_team_size = teams.aggregate(Min("num_members"))["num_members__min"]
        team = teams.filter(num_members__lte=min_team_size).order_by("?").first()
        TeamMember.objects.create(team=team, user=user)
        return team

    def members(self) -> models.QuerySet:
        """Users who are member of this crowd"""
        return User.objects.filter(
            pk__in=TeamMember.objects.filter(team__crowd=self).values_list(
                "user", flat=True
            )
        )

    @classmethod
    def stale_cutoff(cls) -> dt.datetime:
        return now() - dt.timedelta(hours=TEAMPICKER_CROWD_STALE_HOURS)


class Team(models.Model):
    """Team within a crowd."""

    TEAM_NAMES = (
        "Alpha",
        "Bravo",
        "Charlie",
        "Delta",
        "Echo",
        "Foxtrot",
        "Golf",
        "Hotel",
        "India",
        "Juliett",
        "Kilo",
        "Lima",
        "Mike",
        "November",
        "Oscar",
        "Papa",
        "Quebec",
        "Romeo",
        "Sierra",
        "Tango",
        "Uniform",
        "Victor",
        "Whiskey",
        "X-ray",
        "Yankee",
        "Zulu",
    )

    crowd = models.ForeignKey(Crowd, on_delete=models.CASCADE, related_name="teams")
    num = models.PositiveIntegerField(db_index=True)

    def __str__(self) -> str:
        return f"{self.crowd} - {self.name}"

    @property
    def name(self) -> str:
        return self.TEAM_NAMES[self.num]
        # return chr(ord("A") + self.num)

    def members_sorted(self) -> models.QuerySet:
        return self.members.select_related(
            "user", "user__profile__main_character"
        ).order_by("user__username")


class TeamMember(models.Model):
    """Member of a team."""

    team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name="members")
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="+")

    def __str__(self) -> str:
        return f"{self.team} - {self.user}"

    @property
    def name(self) -> str:
        try:
            return self.user.profile.main_character.character_name
        except AttributeError:
            return str(self.user)
