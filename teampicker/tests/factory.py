from itertools import count

from teampicker.models import Crowd


def create_crowd(created_by, **kwargs) -> Crowd:
    num = next_number("crowd")
    params = {"name": f"Test crowd {num}", "num_teams": 3, "created_by": created_by}
    params.update(kwargs)
    return Crowd.objects.create(**params)


def next_number(key=None) -> int:
    if key is None:
        key = "_general"
    try:
        return next_number._counter[key].__next__()
    except AttributeError:
        next_number._counter = dict()
    except KeyError:
        pass
    next_number._counter[key] = count(start=1)
    return next_number._counter[key].__next__()
