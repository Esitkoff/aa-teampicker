from django_webtest import WebTest

from app_utils.testing import create_fake_user

from teampicker.models import Crowd, TeamMember

from .factory import create_crowd


class TestManageCrowds(WebTest):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.user = create_fake_user(
            1001,
            "Bruce Wayne",
            permissions=["teampicker.basic_access", "teampicker.manage_crowds"],
        )

    def test_can_open_page_for_creating_crowd(self):
        # given
        self.app.set_user(self.user)
        response = self.app.get("/teampicker/crowds/manage")
        # when
        response = response.click(linkid="crowd-create-form")
        # then
        self.assertTemplateUsed(response, "teampicker/crowd_form.html")

    def test_can_create_crowd(self):
        # given
        self.app.set_user(self.user)
        response = self.app.get("/teampicker/crowds/add")
        # when
        form = response.forms["crowd_form"]
        form["name"] = "dummy"
        form["num_teams"] = 3
        response = form.submit()
        # then
        self.assertEqual(response.status_code, 302)
        self.assertRegex(response.url, r"\/teampicker\/crowds\/[^\/]*\/detail")
        self.assertTrue(Crowd.objects.filter(name="dummy").exists())

    def test_can_open_page_with_crowd_details(self):
        # given
        crowd = create_crowd(created_by=self.user)
        self.app.set_user(self.user)
        # when
        response = self.app.get(f"/teampicker/crowds/{crowd.pk}/detail")
        # then
        self.assertEqual(response.status_code, 200)


class TestJoinCrowds(WebTest):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.user = create_fake_user(
            1001, "Bruce Wayne", permissions=["teampicker.basic_access"]
        )

    def test_can_open_join_page(self):
        # given
        crowd = create_crowd(created_by=self.user)
        self.app.set_user(self.user)
        # when
        response = self.app.get(f"/teampicker/crowds/{crowd.pk}")
        # then
        self.assertEqual(response.status_code, 200)

    def test_can_join_crowd(self):
        # given
        crowd = create_crowd(created_by=self.user)
        self.app.set_user(self.user)
        response = self.app.get(f"/teampicker/crowds/{crowd.pk}")
        # when
        response = response.click(linkid="crowd-join-team").follow()
        # then
        self.assertEqual(response.status_code, 200)
        self.assertIn(self.user, crowd.members())

    def test_can_join_crowd_multiple_times(self):
        # given
        crowd = create_crowd(created_by=self.user)
        self.app.set_user(self.user)
        response = self.app.get(f"/teampicker/crowds/{crowd.pk}")
        # when
        response = response.click(linkid="crowd-join-team").follow()
        response = response.click(linkid="crowd-join-team").follow()
        # then
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            TeamMember.objects.filter(team__crowd=crowd, user=self.user).count(), 2
        )
