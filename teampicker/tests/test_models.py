from django.test import TestCase

from app_utils.testing import create_fake_user

from .factory import create_crowd


class TeastCrowd(TestCase):
    def test_should_return_members(self):
        # given
        user = create_fake_user(
            1001, "Bruce Wayne", permissions=["teampicker.basic_access"]
        )
        crowd = create_crowd(created_by=user)
        crowd.join_random_team(user)
        # when
        self.assertIn(user, crowd.members())
