from django.urls import path

from teampicker.views import (
    CrowdCreateView,
    CrowdDeleteView,
    CrowdDetailView,
    CrowdJoinView,
    CrowdListView,
    CrowdManageView,
    crowd_clear_view,
    crowd_join_view,
    index_view,
    team_member_delete_view,
)

app_name = "teampicker"

urlpatterns = [
    path("", index_view, name="index"),
    path("crowds/list", CrowdListView.as_view(), name="crowd-list"),
    path("crowds/manage", CrowdManageView.as_view(), name="crowd-manage"),
    path("crowds/add", CrowdCreateView.as_view(), name="crowd-add"),
    path("crowds/<str:pk>/detail", CrowdDetailView.as_view(), name="crowd-detail"),
    path("crowds/<str:pk>/clear", crowd_clear_view, name="crowd-clear"),
    path("crowds/<str:pk>/delete", CrowdDeleteView.as_view(), name="crowd-delete"),
    path("crowds/<str:pk>", CrowdJoinView.as_view(), name="crowd-join"),
    path("crowds/<str:pk>/join", crowd_join_view, name="crowd-join-team"),
    path(
        "team-member/<int:pk>/delete",
        team_member_delete_view,
        name="team-member-delete",
    ),
]
